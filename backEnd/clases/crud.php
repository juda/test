<?php
  /*Esta clase es para manejar las conexiones a las bases de datos*/
  class crud{
  	 protected $host;
  	 protected $user;
  	 protected $password;
  	 protected $db;
  	 protected $port;
  	 protected $link;

  	 public function __construct(){
        include('../backEnd/params/connectParams.php');//se incluyen los parámetros de conexión de la base de datos
        $this->host = $host;
        $this->user = $user;
        $this->password = $password;
        $this->db=$db;
        $this->port=$port;
    }

    /*Metodo que permite conectarse a la base de datos*/
    public function conectar(){
    	$this->link = mysqli_connect($this->host,$this->user,$this->password,$this->db,$this->port);
    	if($this->link){
          $response = $this->link;
    	}
    	else{
          $response = 'Error en el proceso de conexion.';   
    	}
      return $response;
    }

     /*Metodo que permite desconectarse de la base de datos*/
    public function desconectar()
    {
      $this->link->close();
    }

    /*Metodo para hacer consultas a la base de datos, las consultas se hacen con sentencias preparadas si la consulta no lleva parámetros, el parámetro types solo debe contener comillas simples o dobles '' 0 "", de lo contrario el tipo de parámetro que va ejenmplo 'ssi' o "sssi". Por otro lado, en caso de llevar parámetros, el parámetro params, recibe un array no asociativo con los parámetros de la consulta en y si no, simplemente comillas simples o dobles */
    public function getData($query,$types,$params){
       $this->conectar(); //Nos conectamos a la base de datos
       $stmt = $this->link->prepare($query); //se prepara la sentencia
       if($types!='' or $types != ""){//Si la consulta lleva parámetros se validan
        $stmt->bind_param($types,...$params);  //Se validan parámetros
       }
       $stmt->execute(); //Se ejecuta la consulta
       $result = $stmt->get_result(); // se obtiene el resultado

       if ($result->num_rows > 0)
        {
            $data= array(); 
            while($row = $result->fetch_assoc())
            {
              $data[]=$row;
            }

        } //En caso de que la consulta retorne datos, se mandan en un array
        else 
        {
          $data= array('response_code'=>'01','message'=>'Usuario o password Incorrecto');
        } //En caso de que la consulta no retorne datos, se manda un array con dicho mensaje
        $this->desconectar(); //Nos desconectamos de la base de datos
        return $data;
    }

    /*Este método servirá para insertar,actualizar y eliminar registros de la base de datos, recibe el query, los parámetros y el  $setType, el cual indica el tipo de interacción si es insertar, actualizar o eliminar con el fin de retornar el respectivo mensaje */
    public function setData($query,$types,$params,$setType){
      $this->conectar(); //Nos conectamos a la base de datos
       $stmt = $this->link->prepare($query); //se prepara la sentencia
       $stmt->bind_param($types,...$params);  //Se validan parámetros
       $result=$stmt->execute();//Se ejecuta la consulta
       if($result) {
            $data= array('response_code'=>'00','message'=>'Registro '.$setType.' exitosamente'); //Se retorna el mensaje de exito para el tipo de interacción ya sea inserción,actualización o eliminación
        }else{
            $data= array('response_code'=>'01','message'=>$stmt->error); //Se retorna el mensaje de error
        }
        $this->desconectar(); //Nos desconectamos de la base de datos
        return $data;
    }
  }
?>