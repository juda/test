<?php
    include('clases/crud.php');
    //Se validan los campos para guardar en la base de datos y subir al servidor la imagen
	if($_GET['nombre']!='' && $_GET['codigo']!='' && $_GET['ciudad']!='' && $_FILES['foto']['tmp_name']!='' && $_GET['id']!=''){   
    	$crud = new crud();//Se instancia un objeto de la clase crud
	    $img='picture'.date('Ymdhis').'.jpg'; //este es el nombre que se le va a poner a la imagen al momento de subir al servidor
		$data=array($_GET['nombre'],$_GET['codigo'],'images/'.$img,$_GET['ciudad'],$_GET['id']);//Los datos que se pasan en el array al momento de insertat
	    $x=$crud->setData("UPDATE clients SET clients.name=?,clients.code=?,picture=?,city_id=? WHERE id =?","sssii",$data,'actualizado');//sentencia preparada para una consulta de actualización
	    $respuesta = json_encode($x);
	    $info= (array) json_decode(stripslashes($respuesta));
	    if($info['response_code']=='00'){//Si el registro se inserta, se mueve el archivo al servidor.
	        if (move_uploaded_file($_FILES['foto']["tmp_name"], "../images/".$img)) {
	        //more code here...
	               $response=array('message'=>'Cliente Actualizado con éxito.');

			    } else {
			        $response=array('message'=>'Error al momento de subir la imagen.');
			    }  
	    }
	    else{
	      $response=array('message'=>$info['message']); 
	    }
	}
	else{
	    $response=array('message'=>'Verifique que haya digitado el nombre,código,ciudad e imagen.');	
	}    
	    
    print_r(json_encode($response));

?>