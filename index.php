<?php
session_start();
if(isset($_SESSION['user_authorized'])) {session_destroy();}
?>
<?php 
if(isset($_POST['login']) )
{ 
  include('backEnd/clases/crud.php');
  $crud = new crud('backEnd/params/connectParams.php');

  //getSedeEstudiante($id);
  //Logueo de docente o estudiante
  $usuario=$_POST['usuario'];
  $password=md5($_POST['password']);
  $data=array($usuario,$password);
  $respuesta =  $crud->getData("SELECT * FROM users where username = ? and password = ? ",'ss',$data);
  $respuesta = json_encode($respuesta);
  $info= (array) json_decode(stripslashes($respuesta));
  if (property_exists((object)$info, 'response_code')) {
    echo $info['message'];
  }
  else{
     $_SESSION['user_authorized']=true;
     $_SESSION['usuario']=$info[0]->username;
     header("Location: dashboardClients.php");
  }
}
?>   

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Votaciones Uniminuto</title>
    <link href="../css/styles.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/js/all.min.js" crossorigin="anonymous"></script>
</head>
<body>
    <div id="layoutAuthentication">
        <div id="layoutAuthentication_content">
            <main>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-5">
                            <br/>
                            <br/>
                           <div class="card shadow-lg border-0 rounded-lg mt-5">
                               <div class="card-body" id="estProf">
                                <form id="loginForm2" name="loginForm2" method="POST">
                                    <div class="form-group">
                                        <label class="small mb-1" for="inputEmailAddress">Usuario</label>
                                        <input class="form-control py-4"  id="usuario" name="usuario"   placeholder="Usuario" />
                                    </div>
                                    <div class="form-group">
                                        <label class="small mb-1" for="inputPassword">Password</label>
                                        <input class="form-control py-4"  id="password" name="password" type="password" placeholder="Password" />
                                    </div>

                                    <div class="form-group d-flex align-items-center justify-content-between mt-4 mb-0">

                                        <button type="submit" class="btn btn-warning" id="login" name="login">Iniciar sesión</button>
                                    </div>
                                </form>
                            </div>

                           

                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>


    <div id="layoutAuthentication_footer">
        <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; Your Website 2020</div>
                    <div>
                        <a href="#">Privacy Policy</a>
                        &middot;
                        <a href="#">Terms &amp; Conditions</a>
                    </div>
                </div>
            </div>
        </footer>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
<script src="js/scripts.js"></script>

<script type="text/javascript">
    $(document).ready(function()
    {
       

    });
</script>
</body>
</html>
