<div class="nav">
    <div class="sb-sidenav-menu-heading">Core</div>
    <a class="nav-link" href="dashboardClients.php">
        <div class="sb-nav-link-icon"><i class="fa fa-search"></i></div>
        Ver Clientes
    </a>
    <a class="nav-link" href="createClients.php">
        <div class="sb-nav-link-icon"><i class="fas fa-user"></i></div>
        Crear Clientes
    </a>
    <a class="nav-link" href="closeAdmin.php">
        <div class="sb-nav-link-icon"><i class="fas fa-times-circle"></i></div>
        Salir
    </a>
</div>
